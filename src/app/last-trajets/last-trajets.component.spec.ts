import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LastTrajetsComponent } from './last-trajets.component';

describe('LastTrajetsComponent', () => {
  let component: LastTrajetsComponent;
  let fixture: ComponentFixture<LastTrajetsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LastTrajetsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LastTrajetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
