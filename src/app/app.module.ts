import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { ConnexionComponent } from './connexion/connexion.component';
import { InscriptionComponent } from './inscription/inscription.component';
import { AcceuilComponent } from './acceuil/acceuil.component';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { ProfileComponent } from './profile/profile.component';
import { FormsModule } from '@angular/forms';
import { GuardAuthServiceService } from './guards/guard-auth-service.service';
import { AuthService } from './services/auth.service';
import { TrajetsService } from './services/trajets.service';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MatButtonModule} from '@angular/material';
import { MatCardModule } from '@angular/material';
import { MatInputModule } from '@angular/material';
import { MatToolbarModule } from '@angular/material';
import { MatGridListModule } from '@angular/material';
import { HeaderComponent } from './header/header.component';
//import { VisitorComponent } from './visitor/visitor.component';
import { LastTrajetsComponent } from './last-trajets/last-trajets.component';
import { FooterComponent } from './footer/footer.component';
import { TrajetsComponent } from './trajets/trajets.component';
import { AddTrajetComponent } from './add-trajet/add-trajet.component';
let lesRoutes: Routes = [
  { path: '', redirectTo: 'visitor', pathMatch: 'full' },
  { path: 'trajets',component: TrajetsComponent ,canActivate: [GuardAuthServiceService]},
  { path: 'trajets/:id',component: TrajetsComponent ,canActivate: [GuardAuthServiceService]},
 // { path: 'visitor', component: VisitorComponent },
  { path: 'inscription', component: InscriptionComponent },
  { path: 'connexion', component: ConnexionComponent },
  { path: 'acceuil', component: AcceuilComponent ,canActivate: [GuardAuthServiceService]},
  { path: 'profile', component: ProfileComponent ,canActivate: [GuardAuthServiceService]} ,
  { path: 'addTrajet', component: AddTrajetComponent ,canActivate: [GuardAuthServiceService] } 
 // { path: '**', component: PagenotfoundComponent}
]
@NgModule({
  declarations: [
    AppComponent,
    ConnexionComponent,
    InscriptionComponent,
    AcceuilComponent,
    ProfileComponent,
    HeaderComponent,
    LastTrajetsComponent,
  //  VisitorComponent,
    FooterComponent,
    TrajetsComponent,
    AddTrajetComponent
  ],
  imports: [
    BrowserAnimationsModule,MatCardModule,MatButtonModule,MatInputModule,MatToolbarModule,MatGridListModule,
    FormsModule,
    HttpModule,
    BrowserModule,
    RouterModule.forRoot(lesRoutes)
  ],
  providers: [ 
  AuthService,
  GuardAuthServiceService,
  TrajetsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
