export class Trajet {
    _id: String;
    userID: String;
    depart: String;
    arrivee: String;
    rdvDepart: String;
    rdvDepose: String;
    date: Date;
    distance: Number;
    prix: Number;
}