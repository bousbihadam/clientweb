import { Component, OnInit } from '@angular/core';
import { Trajet } from '../Trajet';
import { TrajetsService } from '../services/trajets.service';
import { Router,ActivatedRoute} from '@angular/router';
import { MatCardModule } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-trajets',
  templateUrl: './trajets.component.html',
  styleUrls: ['./trajets.component.css']
})
export class TrajetsComponent implements OnInit {
  
  trajetById: any[];
  //trajetById: Trajet[];
  mapUrl: string;
  src:any;
  constructor(private trajetService: TrajetsService,public router:Router,public route:ActivatedRoute,public sanitizer: DomSanitizer) {}
   
  ngOnInit() {  
   
   
    var id = this.route.snapshot.params.id;
      this.trajetService.getTrajetById(id).subscribe(trajets => {
        console.log("HEHEHEHEHEHEHEHEHEHEHEHEHEHEHEHEHEHEHEHEHEHEHEHE");
  
        this.trajetById = Array.of(trajets);  
 
        this.mapUrl= "https://www.google.com/maps/embed/v1/directions?key=AIzaSyA0mv2OBqt8h5AwHKblvl4v78hyLkv98is"
        +"&origin="+this.trajetById[0].trajets.depart
        +"&destination="+this.trajetById[0].trajets.arrivee;
        
        this.src = this.sanitizer.bypassSecurityTrustResourceUrl(this.mapUrl);
        
      });
    }
    
} 
 
 
