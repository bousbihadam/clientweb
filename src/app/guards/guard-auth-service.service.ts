import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Injectable()
export class GuardAuthServiceService implements CanActivate {
  constructor(public authService: AuthService,public router: Router){}


canActivate() {
    if (this.authService.isloggedIn()) {
        return true;
    } else {
        this.router.navigate(['/connexion']);
        return false;
    }
}
}
