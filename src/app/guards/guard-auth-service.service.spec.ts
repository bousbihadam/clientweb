import { TestBed, inject } from '@angular/core/testing';

import { GuardAuthServiceService } from './guard-auth-service.service';

describe('GuardAuthServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GuardAuthServiceService]
    });
  });

  it('should be created', inject([GuardAuthServiceService], (service: GuardAuthServiceService) => {
    expect(service).toBeTruthy();
  }));
});
