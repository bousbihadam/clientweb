import { Component, OnInit } from '@angular/core';
import { Trajet } from '../Trajet';
import { TrajetsService } from '../services/trajets.service';
import { Router,ActivatedRoute } from '@angular/router';
import { MatCardModule } from '@angular/material';
import { MatGridListModule } from '@angular/material'; 


@Component({
  selector: 'app-root',
  templateUrl: './acceuil.component.html',
  styleUrls: ['./acceuil.component.css']
})
export class AcceuilComponent implements OnInit {
  
    listeTrajets: Trajet[];
    constructor(private trajetService: TrajetsService,public router:Router,public route:ActivatedRoute) {}
  
    ngOnInit() {
      this.trajetService.getallTrajets().subscribe(trajets => {
        console.log("HAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAE");
        this.listeTrajets = trajets;
        console.log(this.listeTrajets);
      });
    }
    
}


