import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service'
import { Router } from '@angular/router';
import {MatButtonModule} from '@angular/material';
import { MatCardModule } from '@angular/material';
import { MatInputModule } from '@angular/material';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.css']
})
export class ConnexionComponent implements OnInit {

  private email: string;
  private password: string;
  constructor(public authService: AuthService,public router:Router) { }


  onConnexionSubmit() {
    let user = {
        email: this.email,
        password: this.password
  }
  this.authService.connexion(user).subscribe(data => {
    if (data.allowed) {
      localStorage.setItem('token', data.token);
      localStorage.setItem('userID', data.userID);
      this.router.navigate(['acceuil']);
    } else {
      this.router.navigate(['connexion']);
    }
  });
}
  


  ngOnInit() {
  }

}
