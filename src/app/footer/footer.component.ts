import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Router } from '@angular/router';
import { MatCardModule } from '@angular/material';
import { MatGridListModule } from '@angular/material'; 
import { AuthService } from '../services/auth.service';
import { Trajet } from '../Trajet';
import { TrajetsService } from '../services/trajets.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
 
  derniersTrajets: Trajet[];
  constructor(public authService: AuthService,private lastTrajets: TrajetsService,public router:Router) {}


  ngOnInit() {
    this.lastTrajets.getlastTrajets().subscribe(trajets => {
      this.derniersTrajets = trajets;
    });
  }

}
 