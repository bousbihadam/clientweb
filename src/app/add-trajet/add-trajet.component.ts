import { Component, OnInit } from '@angular/core';
import { TrajetsService } from '../services/trajets.service';
import { Router,ActivatedRoute } from '@angular/router';
import { Trajet } from '../Trajet';
 
@Component({
  selector: 'app-add-trajet',
  templateUrl: './add-trajet.component.html',
  styleUrls: ['./add-trajet.component.css']
})
export class AddTrajetComponent implements OnInit {

  
  private depart: String;
  private arrivee: String;
  private rdvDepart: String;
  private rdvDepose: String;
  private date: Date;
  private distance: Number;
  private prix: Number;

  newTrajet : Trajet;
  
  constructor(public trajetsService: TrajetsService,public router:Router,public route:ActivatedRoute) { }

  onAddTrajetSubmit() {
    let userID = localStorage.getItem('userID');
    console.log(userID);
    let Trajet = {
      userID: userID,
      depart: this.depart,
      arrivee: this.arrivee,
      rdvDepart: this.rdvDepart,
      rdvDepose: this.rdvDepose,
      date: this.date,
      distance: this.distance,
      prix: this.prix
  }
     
  this.trajetsService.addTrajet(Trajet).subscribe(res => {
      if(res.allowed){
        this.router.navigate(['acceuil']);
      }
      else {
        this.router.navigate(['pagenotfound']);
      }
    });
  }

  ngOnInit() {
  }

}
