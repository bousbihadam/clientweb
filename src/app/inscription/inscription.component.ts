import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service'
import { Router } from '@angular/router';
import {MatButtonModule} from '@angular/material';
import { MatCardModule } from '@angular/material';
import { MatInputModule } from '@angular/material';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.css']
})
export class InscriptionComponent implements OnInit {

  private email: string;
  private password: string;
  private nom: string;
  private prenom: string;
  private telephone: number;
  private ville: string;
  constructor(public authService: AuthService,public router:Router) { }


  onInscriptionSubmit() {
    let user = {
        email: this.email,
        password: this.password,
        nom: this.nom,
        prenom: this.prenom,
        telephone: this.telephone,
        ville: this.ville
  }
  this.authService.inscription(user).subscribe(data => {
    if (data.allowed) {
      localStorage.setItem('token', data.token);
      localStorage.setItem('userID', data.userID);
      this.router.navigate(['acceuil']);
    } else {
      this.router.navigate(['connexion']);
    }
  });
  }
  ngOnInit() {
  }

}


