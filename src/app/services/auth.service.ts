import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { tokenNotExpired } from 'angular2-jwt';

@Injectable()
export class AuthService {

  user: any;

  constructor(private http: Http) { }


  connexion(user) {
    return this.http.post('http://localhost:3000/connexion', user)
      .map(res => res.json());
  }

  isloggedIn() {
    return tokenNotExpired();
  }

  logout() {
    localStorage.clear();
  }

  inscription(user) {
    return this.http.post('http://localhost:3000/inscription', user)
      .map(res => res.json());
  }
}
 