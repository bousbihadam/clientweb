import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class TrajetsService {
  constructor(private http: Http) { }

  /* les Derniers Trajets */
  getlastTrajets() {
    return this.http.get('http://localhost:3000/lastTrajets').map(res => res.json().trajets);
  }
  getTrajetById(trajetID){
    return this.http.get('http://localhost:3000/trajets/'+trajetID).map(result => result.json());
  }
  getallTrajets() {
    return this.http.get('http://localhost:3000/allTrajets').map(result => result.json().trajets);
  }
  addTrajet(trajet) {
    return this.http.post('http://localhost:3000/addTrajet', trajet).map(res => res.json());
  }
}

 