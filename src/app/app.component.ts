import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Trajet } from './Trajet';
import { TrajetsService } from './services/trajets.service';
import { AuthService } from './services/auth.service';
import { Router } from '@angular/router';
import { MatCardModule } from '@angular/material';
import { MatGridListModule } from '@angular/material';
import { HeaderComponent } from './header/header.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {

  constructor() {}

  ngOnInit() {
  }
  

}
